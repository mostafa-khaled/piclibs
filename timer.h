#ifndef TIMER_H_
#define TIMER_H_

#include "types.h"

#define RTCC_INTERNAL 0x00
#define RTCC_EXT_L_TO_H 0x20
#define RTCC_EXT_H_TO_L 0x30

#define RTCC_DIV_2 0x08
#define RTCC_DIV_4 0x09
#define RTCC_DIV_8 0x0a
#define RTCC_DIV_16 0x0b
#define RTCC_DIV_32 0x0c
#define RTCC_DIV_64 0x0d
#define RTCC_DIV_128 0x0e
#define RTCC_DIV_256 0x0f

// Sets the source, prescale, etc. for timer0
void setup_timer_0(uint8 mode);

// Initializes timer0 clock/counter
void set_timer0(uint8 value);

// Initializes timer0 clock/counter
void set_rtcc(uint8 value);

// Returns the value of the timer0 clock/counter
uint8 get_timer0();

#define T1_DISABLED 0x00
#define T1_INTERNAL 0x01
#define T1_EXTERNAL 0x07
#define T1_EXTERNAL_SYNC 0x03

#define T1_DIV_BY_1 0x00
#define T1_DIV_BY_2 0x10
#define T1_DIV_BY_4 0x20
#define T1_DIV_BY_8 0x30

#define T1_CLK_OUT 0x08

// Disables or sets the source and prescale for timer1
void setup_timer_1(uint8 mode);

// Initializes the timer1 clock/counter
void set_timer1(uint16 value);

// Returns the value of the timer1 clock/counter
uint16 get_timer1();

#define T2_DISABLED 0x00
#define T2_INTERNAL 0x04

#define T2_DIV_BY_1 0x00
#define T2_DIV_BY_4 0x01
#define T2_DIV_BY_16 0x02

// Disables or sets the prescale, period and a postscale for timer2
void setup_timer_2(uint8 mode, uint8 period, uint8 postscale);

// Initializes the timer2 clock/counter
void set_timer2(uint8 value);

// Returns the value of the timer2 clock/counter
uint8 get_timer2();

#endif // TIMER_H_
