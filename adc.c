#include "adc.h"

#if defined(_16F88)

#if !defined(ADC_BITS)
#warning "ADC_BITS not defined, assuming 10"
#define ADC_BITS 10
#endif


// these are private defines
#define ADC_ADON (0b00000000000000001)
#define ADC_RIGHT_JUSTIFIED (0b10000000000000000)
#define ADC_LEFT_JUSTIFIED (0b00000000000000000)

void setup_adc(uint16 mode) {
  ADCON0 = (mode | ADC_ADON | ADC_LEFT_JUSTIFIED) & 0xFF;
  ADCON1 = ((mode | ADC_ADON | ADC_LEFT_JUSTIFIED) >> 8) & 0xFF;
}

void setup_adc_ports(uint16 ports) {
  ANSEL = ports & 0xFF;
  ADCON1 = (ports >> 8) & 0xFF;
}

void set_adc_channel(int8 channel) {
  ADCON0 &= 0b11000111;
  ADCON0 |= channel << 3;
}

static void adc_read_start() { GO = 1; }

static uint16 adc_read_only() {
  while (nDONE)
    ;
  return ADRESH << 8 | ADRESL;
}

uint16 read_adc() {
  adc_read_start();

  return adc_read_only() >> (16 - ADC_BITS);
}

#endif
