FREQ:=4000000
CPU:=16F628a
CFLAGS= -D_XTAL_FREQ=${FREQ} -mcpu=${CPU} -DGETC_IS_UART_GETC -DPUTC_IS_UART_PUTC -mconst-data-in-progmem

CC=xc8-cc
CC=$(wildcard /opt/microchip/xc8/*/bin/xc8-cc)
CFLAGS=-D_XTAL_FREQ=$(FREQ) -mcpu=$(CPU) -I.
SHELL:=/bin/bash

.ONESHELL:
clear:
	@for file in examples/*
	do
		[[ "$$file" =~ ^[^.]*$$ ]] && continue
		for suffix in h c hex txt md
		do
			pattern='^.*\.'"$$suffix"'$$'
			[[ "$$file" =~ $$pattern ]] && continue 2
		done
		printf 'rm %s\n' "$$file"
		rm "$$file"
	done

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%:
	$(CC) $(CFLAGS) ./examples/$@.c *.c -o ./examples/$@.hex

.PHONY: clear
