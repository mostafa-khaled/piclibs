#ifndef PRELUDE_H_
#define PRELUDE_H_

#include <stdint.h>
#include <stdlib.h>
#include <xc.h>

#include "delay.h"
#include "gpio.h"
#include "uart.h"
#include "adc.h"
#include "timer.h"
#include "types.h"

#endif // PRELUDE_H_
