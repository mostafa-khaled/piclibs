#ifndef ADC_H_
#define ADC_H_

#include <xc.h>

#if defined(_16F88)
#include <types.h>

#define ADC_CH0 (1 << 0)
#define ADC_CH1 (1 << 1)
#define ADC_CH2 (1 << 2)
#define ADC_CH3 (1 << 3)
#define ADC_CH4 (1 << 4)
#define ADC_CH5 (1 << 5)
#define ADC_CH6 (1 << 6)
#define AN0 (ADC_CH0)
#define AN1 (ADC_CH1)
#define AN2 (ADC_CH2)
#define AN3 (ADC_CH3)
#define AN4 (ADC_CH4)
#define AN5 (ADC_CH5)
#define AN6 (ADC_CH6)
#define sAN0 (AN0)
#define sAN1 (AN1)
#define sAN2 (AN2)
#define sAN3 (AN3)
#define sAN4 (AN4)
#define sAN5 (AN5)
#define sAN6 (AN6)

#define VSS_VDD   (0b00 << 12)
#define VREF_VDD  (0b01 << 12 | AN2)
#define VSS_VREF  (0b10 << 12 | AN3)
#define VREF_VREF (0b11 << 12 | AN2 | AN3)

#define ALL_ANALOG (0b111111)

void setup_adc_ports(uint16 ports);

// mode is the concatination of `ADCON1` and `ADCON0`.
#define ADC_CLOCK_DIV_2     (0b0000000000000000)
#define ADC_CLOCK_DIV_4     (0b0100000000000000)
#define ADC_CLOCK_DIV_8     (0b0000000001000000)
#define ADC_CLOCK_DIV_16    (0b01000000010000000)
#define ADC_CLOCK_DIV_32    (0b00000000100000000)
#define ADC_CLOCK_DIV_64    (0b01000000100000000)
#define ADC_CLOCK_INTERNAL  (0b01000000110000000)

void setup_adc(uint16 mode);
void set_adc_channel(int8 channel);

uint16 read_adc();

#endif

#endif // ADC_H_
