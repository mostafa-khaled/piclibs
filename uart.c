#include "uart.h"
#include "prelude.h"

#define UART_RECEIVE (1 << 0)
#define UART_TRANSMIT (1 << 1)

#define FOSC _XTAL_FREQ
#define MAX_LOW_SPEED_BAUD_RATE (FOSC / (64 * 256))
#define MAX_HIGH_SPEED_BAUD_RATE (FOSC / (16 * 256))

typedef struct {
  // a cursor to the last byte sent
  uint8 head;
  // a cursor to the last byte inserted
  uint8 tail;
  uint8 data[UART_SINGLE_BUFFER_SIZE];
} cyclic_buffer_t;

static cyclic_buffer_t RECEIVE_BUFFER;
static cyclic_buffer_t TRANSMIT_BUFFER;

static inline uint8 cyclic_buffer_len(cyclic_buffer_t buffer) {
  if (buffer.tail >= buffer.head) {
    return buffer.tail - buffer.head;
  } else {
    return UART_SINGLE_BUFFER_SIZE - buffer.head + buffer.tail;
  }
}

static inline int8 cyclic_buffer_is_full(cyclic_buffer_t buffer) {
  return (cyclic_buffer_len(buffer) == UART_SINGLE_BUFFER_SIZE ? 1 : 0);
}

static inline void inc_tail_cursor(cyclic_buffer_t *buffer) {
  buffer->tail++;
  if (buffer->tail == UART_SINGLE_BUFFER_SIZE) {
    buffer->tail = 0;
  }
}

static inline void inc_head_cursor(cyclic_buffer_t *buffer) {
  buffer->head++;
  if (buffer->head == UART_SINGLE_BUFFER_SIZE) {
    buffer->head = 0;
  }
}

// puts the data into the buffer discarding the oldest byte if it was full.
static void cyclic_buffer_put(cyclic_buffer_t *buffer, uint8 data) {
  inc_tail_cursor(buffer);
  buffer->data[buffer->tail] = data;
}

// reads the oldest byte from the buffer.
static uint8 cyclic_buffer_get(cyclic_buffer_t *buffer) {
  if (buffer->tail != buffer->head) {
    inc_head_cursor(buffer);
    return buffer->data[buffer->head];
  } else {
    return -1;
  }
}

static inline void set_low_speed() { BRGH = 0; }

static inline void set_high_speed() { BRGH = 1; }

static inline void set_speed(int32 baudrate) {
  if (baudrate > MAX_LOW_SPEED_BAUD_RATE) {
    set_high_speed();
    SPBRG = FOSC / (16 * baudrate) - 1;
  } else {
    set_low_speed();
    SPBRG = FOSC / (64 * baudrate) - 1;
  }
}

static inline void init_tx() {
  TRISB2 = 1;
  TXIE = 1;
  TXEN = 1;
}

static inline void write_byte(uint8 b) { TXREG = b; }

static inline void init_rx() {
  TRISB1 = 1;
  RCIE = 1;
  CREN = 1;
}

void uart_init(int32 baudrate) {
  set_speed(baudrate);
  init_tx();
  init_rx();
  SYNC = 0;
  SPEN = 1;
  GIE = 1;
  PEIE = 1;
}

void setup_uart(int32 baud) { uart_init(baud); }

void setup_uart_speed(int32 baud) { uart_init(baud); }

uint8 rcv_buffer_bytes() { return cyclic_buffer_len(RECEIVE_BUFFER); }

uint8 tx_buffer_bytes() { return cyclic_buffer_len(TRANSMIT_BUFFER); }

int rcv_buffer_full() { return cyclic_buffer_is_full(RECEIVE_BUFFER); }

int tx_buffer_full() { return cyclic_buffer_is_full(TRANSMIT_BUFFER); }

void uart_putc(char c) {
  cyclic_buffer_put(&TRANSMIT_BUFFER, c);
  if (cyclic_buffer_len(TRANSMIT_BUFFER) == 1) {
    RCIF = 1;
  }
}

void uart_puts(char *s) {
  while (*s) {
    uart_putc(*s);
    s++;
  }
}

char uart_getc() {
  while (cyclic_buffer_len(RECEIVE_BUFFER) == 0)
    ;
  return cyclic_buffer_get(&RECEIVE_BUFFER);
}

// receive interrupt handler
#pragma INT_TX
void tx_isr() {
  if (cyclic_buffer_len(RECEIVE_BUFFER) < UART_SINGLE_BUFFER_SIZE) {
    cyclic_buffer_put(&RECEIVE_BUFFER, RCREG);
  }
}

#pragma INT_RC
void rc_isr() {
  if (cyclic_buffer_len(TRANSMIT_BUFFER) > 0) {
    write_byte(cyclic_buffer_get(&TRANSMIT_BUFFER));
  }
}

#if defined(PUTC_IS_UART_PUTC)
void putc(char c) { uart_putc(c); }
#endif

#if defined(GETC_IS_UART_GETC)
char getc() { return uart_getc(); }
#endif
