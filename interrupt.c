#include "interrupt.h"
#include "prelude.h"
#include "timer.h"

void enable_interrupts(uint8 interrupts) {
  if (interrupts & TIMER0) {
    T0IE = 1;
  }
  if (interrupts & TIMER1) {
    TMR1IE = 1;
  }
  if (interrupts & TIMER2) {
    TMR2IE = 1;
  }
  if (interrupts & RB) {
    RBIE = 1;
  }
  if (interrupts & TX) {
    TXIE = 1;
  }
  if (interrupts & RC) {
    RCIE = 1;
  }
  if (interrupts & GLOBAL) {
    GIE = 1;
    // hmm...
    PEIE = 1;
  }
}

void disable_interrupts(uint8 interrupts) {
  if (interrupts & TIMER0) {
    T0IE = 0;
  }
  if (interrupts & TIMER1) {
    TMR1IE = 0;
  }
  if (interrupts & TIMER2) {
    TMR2IE = 0;
  }
  if (interrupts & RB) {
    RBIE = 0;
  }
  if (interrupts & TX) {
    TXIE = 0;
  }
  if (interrupts & RC) {
    RCIE = 0;
  }
  if (interrupts & GLOBAL) {
    GIE = 0;
    // hmm...
    PEIE = 0;
  }
}

int8 interrupts_enabled(uint8 interrupt) {
  if (interrupt & TIMER0) {
    return T0IE;
  }
  if (interrupt & TIMER1) {
    return TMR1IE;
  }
  if (interrupt & TIMER2) {
    return TMR2IE;
  }
  if (interrupt & RB) {
    return RBIE;
  }
  if (interrupt & TX) {
    return TXIE;
  }
  if (interrupt & RC) {
    return RCIE;
  }
  if (interrupt & GLOBAL) {
    return GIE;
  }
  return 0;
}

void ext_int_edge(uint8 edge) {
  INTEDG = edge;
  enable_interrupts(EXT);
}

int8 interrupt_active(uint8 interrupt) {
  if (interrupt & TIMER0) {
    return T0IF;
  }
  if (interrupt & TIMER1) {
    return TMR1IF;
  }
  if (interrupt & TIMER2) {
    return TMR2IF;
  }
  if (interrupt & RB) {
    return RBIF;
  }
  if (interrupt & TX) {
    return TXIF;
  }
  if (interrupt & RC) {
    return RCIF;
  }
  return 0;
}
