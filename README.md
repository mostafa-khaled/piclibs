# piclibs


## Install

```
cargo install --path bxc8
```

## usage

```
bxc8 new blink # this creates a new project at ./blink
cd blink
bxc8 compile -i main.c # this outputs main.hex file
```
