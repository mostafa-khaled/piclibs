#ifndef DELAY_H_
#define DELAY_H_

#include <builtins.h>

// blocking delay.
#define delay_ms(x) _delay((uint32)((x)*(_XTAL_FREQ/4000.0)))

// blocking delay.
#define delay_us(x) _delay((uint32)((x)*(_XTAL_FREQ/4000000.0)))

#endif  // DELAY_H_
