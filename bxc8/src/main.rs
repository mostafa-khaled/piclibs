//! A compiler front-end for xc8-cc.
//! adds some syntax suger for the compiler
//! Implemented features:
//! 1. `#pragma INT_<interrupt>`: for writing interrupt handlers.
//! 1. `#pragma use delay (clock=<clock>)`
//! 1. `#pragma use FAST_IO(<PORT>)`
//! 1. `#pragma device adc=xx`
//! 1. `#pragma fuses <fuse>[, <fuse>]*`

#![allow(unused)]
use clap::CommandFactory;
use clap::Parser;
use std::{fs, io, path::Path};

mod driver;
mod project_builder;

mod pragmas;

#[derive(Debug)]
enum Error {
    Io(io::Error),
    XC8NotFound,
    InvalidInterruptName(Vec<String>),
    UnknownUsePatterns(Vec<String>),
    UnknownFuses(Vec<String>),
    CouldNotInferTheDevice,
}

type Result<T> = std::result::Result<T, Error>;

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Self::Io(err)
    }
}

/// a module that stores the c code into constant variables
fn main() -> Result<()> {
    use driver::*;
    use project_builder::*;
    match Cli::parse() {
        Cli {
            generator: Some(shell),
            ..
        } => {
            print_completions(shell, &mut Cli::command());
        }
        Cli {
            command: Some(CliCommand::New { name }),
            device,
            ..
        } => {
            let device = device.ok_or(Error::CouldNotInferTheDevice)?;
            fs::create_dir_all(&name)?;
            fs::write(
                name.join("main").with_extension("c"),
                DEFAULT_MAIN
                    .to_string()
                    .replace("prelude.h", &format!("{device}.h")),
            )?;
            let header_dir = name.join("include");
            fs::create_dir_all(&header_dir);
            dump_header_files(&header_dir, &device)?;

            let compile_flags_path = name.join("compile_flags.txt");
            let mut compile_flags = format!("{FIXED_CLANG_FLAGS}\n-Iinclude\n-D_{device}\n", device = device.to_uppercase());
            if let Some(xc8_root) = xc8_root()? {
                compile_flags.extend(
                    xc8_include_dirs(xc8_root)
                        .iter()
                        .filter_map(|dir| dir.to_str().map(|dir| format!("-I{}\n", dir))),
                );
            }

            fs::write(compile_flags_path, compile_flags)?;
        }
        Cli {
            command:
                Some(CliCommand::Compile {
                    input,
                    output,
                    build_dir,
                    ..
                }),
            xc8,
            device,
            ..
        } => {
            let xc8 = xc8
                .or(xc8_root()?.map(|root| root.join("bin").join("xc8-cc")))
                .ok_or(Error::XC8NotFound)?;
            fs::create_dir_all(&build_dir)?;
            let output = output
                .or_else(|| {
                    input
                        .file_stem()
                        .map(|stem| Path::new(stem).with_extension("hex"))
                })
                .unwrap_or("output.hex".into());

            if let Some(device) = device {
                compile_main(input, output, xc8, &build_dir, &device)?;
                return Ok(());
            }

            let main_src = fs::read_to_string(&input)?;
            let device = regex::Regex::new(r#"#include <(\w+)>|#include "(\w+)"#)
                .unwrap()
                .captures(&main_src)
                .map(|matched| matched.get(1).or(matched.get(2)).unwrap().as_str());

            if let Some(device) = device {
                compile_main(input, output, xc8, &build_dir, device)?;
                return Ok(());
            }

            Err(Error::CouldNotInferTheDevice)?
        }
        _ => {}
    }
    Ok(())
}
