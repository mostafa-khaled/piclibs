pub mod fuses;
pub mod interrupt;

/// contains pragmas the get converted to additional compilation flags
pub mod pragma_to_flags;
