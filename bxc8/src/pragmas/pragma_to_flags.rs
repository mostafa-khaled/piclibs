use lazy_static::lazy_static;
use regex::{Captures, Regex};

fn delay_flags(cap: Captures) -> Vec<String> {
    vec![format!("-D_XTAL_FREQ={}", cap.get(1).unwrap().as_str())]
}

fn fast_io_flags(cap: Captures) -> Vec<String> {
    vec![format!(
        "-DFAST_IO_{}",
        cap.get(1).unwrap().as_str().to_uppercase()
    )]
}

fn adc_flags(cap: Captures) -> Vec<String> {
    vec![format!("-DADC_BITS={}", cap.get(1).unwrap().as_str())]
}

lazy_static! {
    static ref DELAY_PATTERN: Regex =
        Regex::new(r"^\s*#\s*pragma\s+(?i:use\s+delay\s*)\(\s*(?i:clock)\s*=\s*([0-9]+)\s*\)").unwrap();
    static ref FAST_IO_PATTERN: Regex =
        Regex::new(r"^\s*#\s*pragma\s+(?i:use\s+fast_io\s*)\(\s([a-f])\s*\)").unwrap();
    static ref ADC_PATTERN: Regex =
        Regex::new(r"^\s*#\s*pragma\s+(?i:device\s+adc)\s*=\s*([0-9]+)").unwrap();
}

pub fn get_pragma_compile_flags(code: &str) -> Vec<String> {
    DELAY_PATTERN
        .captures_iter(code)
        .flat_map(delay_flags)
        .chain(FAST_IO_PATTERN.captures_iter(code).flat_map(fast_io_flags))
        .chain(ADC_PATTERN.captures_iter(code).flat_map(adc_flags))
        .collect()
}

#[cfg(test)]
mod test {
    #[test]
    fn delay_flags() {
        use super::get_pragma_compile_flags;
        let flags = get_pragma_compile_flags(
            r#"
  #pragma use delay( clock = 4000)
"#,
            // FIXME: this works only with a single match I do not know how
            // # pragma use delay(clock=4000)
            // #pragma use delay     ( clock= 4000)
            // #pragma use delay( clock = 4000)
        );
        let flag = "-D_XTAL_FREQ=4000";
        assert_eq!(flags, vec![flag])
    }

    #[test]
    fn adc_falgs() {
        use super::get_pragma_compile_flags;
        let flags = get_pragma_compile_flags(r#"#pragma device aDc=10"#);
        assert_eq!(vec!["-DADC_BITS=10"], flags)
    }
}

