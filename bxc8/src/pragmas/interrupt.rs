use crate::{Error, Result};
/// a map from the interrupt name to the interrupt flag
const INTERRUPT_SOURCES_FLAGS: [(&str, &str); 7] = [
    // external interrupt (pin RB0)
    ("EXT", "INTF"),
    // interrupt on change
    ("RB", "RBIF"),
    // timer0 interrupt
    ("TIMER0", "T0IF"),
    // timer1 interrupt
    // NOTE: this is not a typo
    ("TIMER1", "TMR1IF"),
    // timer2 interrupt
    ("TIMER2", "TMR2IF"),
    // transmit interrupt
    ("TX", "TXIF"),
    // receive interrupt
    ("RC", "RCIF"),
];

/// builds the interrupt service handler with xc8's syntax
/// takes an iterator of (interrupt_name, interrupt_handler).
/// void __interrupt() isr() { .. }
/// returns an error if any of the given interrupt names is invalid
/// FIXME: check for collisions in the inputs
pub(crate) fn build_interrupt_handler<T1: AsRef<str>, T2: AsRef<str>>(
    interrupt_funcs: impl Iterator<Item = (T1, T2)>,
) -> Result<String> {
    let header = "void __interrupt() __auto_generated_isr() {";
    let footer = "}";
    let body = interrupt_funcs
        .map(|(int_name, handler)| {
            INTERRUPT_SOURCES_FLAGS
                .iter()
                .copied()
                .find_map(|(name, flag)| {
                    if name == int_name.as_ref() {
                        Some(format!(
                            r#"  if ({flag} == 1) {{
                                    {handler}();
                                    {flag} = 0;
                                }}
                                "#,
                            handler = handler.as_ref(),
                        ))
                    } else {
                        None
                    }
                })
                .ok_or(int_name.as_ref().to_string())
        })
        .fold(
            Ok(String::new()),
            |acc: std::result::Result<_, Vec<_>>, res| match (acc, res) {
                (Ok(mut acc), Ok(res)) => {
                    acc.push_str(&res);
                    Ok(acc)
                }
                (Err(mut acc), Err(res)) => {
                    acc.push(res);
                    Err(acc)
                }
                (Ok(_), Err(res)) => Err(vec![res]),
                (Err(mut acc), Ok(_)) => Err(acc),
            },
        )
        .map_err(Error::InvalidInterruptName)?;
    Ok(format!("{header}\n{body}{footer}"))
}

/// builds the interrupt document which contains the interrupt handler and whatever requires to
/// make it work.
pub(crate) fn generate_doc(handlers: Vec<(String, String)>) -> Result<String> {
    let includes = "#include <xc.h>";
    let extern_decls = handlers
        .iter()
        .map(|(_, handler)| format!("extern void {handler}();\n",))
        .collect::<String>();
    let generated_handler = build_interrupt_handler(handlers.into_iter())?;
    Ok(format!("{includes}\n{extern_decls}\n{generated_handler}"))
}

/// collects any functions that is defined to be an interrupt handler
/// using the `#pragma INT_<interrupt>` syntax.
/// TODO: return errors instead of panicing on syntax errors
pub fn get_int_handlers(code: impl AsRef<str>) -> Vec<(String, String)> {
    code.as_ref()
        .split("#pragma INT_")
        // the first element is not a match
        .skip(1)
        .map(|seg| {
            let (interrupt_name, seg) = seg.split_once(['\n', ' ', '\t']).unwrap();
            assert!(!interrupt_name.is_empty());
            let seg = seg.trim_start();
            // skip `void`
            let (void, seg) = seg.split_once(['\n', ' ', '\t']).unwrap();
            assert_eq!(void, "void");
            let seg = seg.trim_start();
            (
                interrupt_name.to_string(),
                seg.split_once(|c: char| !(c.is_alphanumeric() || c == '_'))
                    .unwrap()
                    .0
                    .to_string(),
            )
        })
        .collect()
}

mod test {
    use super::*;
    #[test]
    fn handler_builder() {
        let input = vec![("EXT", "ext_isr"), ("RB", "rb_isr")];

        let handwritten_handler = r"void __interrupt() __auto_generated_isr() {
            if (INTF == 1) {
                ext_isr();
                INTF = 0;
            }
            if (RBIF == 1) {
                rb_isr();
                RBIF = 0;
            }
        }";
        let isr_func = build_interrupt_handler(input.into_iter()).unwrap();

        isr_func
            .split_whitespace()
            .zip(handwritten_handler.split_whitespace())
            .for_each(|(gennerated, handwritten)| {
                assert_eq!(gennerated, handwritten);
            });
    }

    #[test]
    fn find_handlers() {
        let input = r"
#pragma INT_EXT
            void ext_isr() {
                // do something
            }
        ";
        assert_eq!(
            get_int_handlers(input),
            vec![("EXT".to_string(), "ext_isr".to_string())]
        );

        let input = r"
            void non_isr() {
            }

#pragma INT_RB
        void rb_isr() {
        }

#pragma INT_EXT
        void ext_isr() {
        }";
        assert_eq!(
            get_int_handlers(input),
            vec![
                ("RB".to_string(), "rb_isr".to_string()),
                ("EXT".to_string(), "ext_isr".to_string())
            ]
        );
    }
}
