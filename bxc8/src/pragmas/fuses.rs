use regex::Regex;

const BOOLEAN_FUSES: [(&str, &str); 6] = [
    ("MCLR", "MCLRE"),
    ("LVP", "LVP"),
    ("CPD", "CPD"),
    ("WDT", "WDTE"),
    ("PROTECT", "CP"),
    ("BROWNOUT", "BOREN"),
];

fn make_xc8_pragma(word: &str) -> Option<String> {
    BOOLEAN_FUSES
        .iter()
        .copied()
        .find_map(|(keyword, xc8_cfg)| {
            if keyword == word {
                Some(format!("#pragma config {} = ON", xc8_cfg))
            } else if keyword == word.trim_start_matches("NO") {
                Some(format!("#pragma config {} = OFF", xc8_cfg))
            } else {
                None
            }
        })
        .or_else(|| match word {
            "HS" => Some("#pragma config FOSC = HS".to_string()),
            "XT" => Some("#pragma config FOSC = XT".to_string()),
            "RC" => Some("#pragma config FOSC = EXTRCCLK".to_string()),
            _ => None,
        })
}

/// generates c code that is equivelent to the fuses in the code.
pub fn get_fuses(code: &str) -> Result<String, Vec<String>> {
    Regex::new(r"#\s*pragma\s+fuses\s+(\w+(\s*,\s*\w+)*)")
        .unwrap()
        .captures_iter(code)
        .flat_map(|captured| {
            captured
                .get(1)
                .unwrap()
                .as_str()
                .split(',')
                .map(|slice| slice.trim())
                .map(|slice| make_xc8_pragma(slice).ok_or(slice))
        })
        .fold(Ok(String::new()), |acc: Result<_, Vec<_>>, new| {
            match (acc, new) {
                (Ok(mut acc), Ok(new)) => {
                    acc.push_str(&new);
                    acc.push('\n');
                    Ok(acc)
                }
                (Err(mut acc), Err(new)) => {
                    acc.push(new.to_string());
                    Err(acc)
                }
                (Err(acc), Ok(_)) => Err(acc),
                (Ok(_), Err(new)) => Err(vec![new.to_string()]),
            }
        })
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn cfg_single() {
        let code = "#pragma fuses HS";
        assert_eq!(get_fuses(code).unwrap(), "#pragma config FOSC = HS\n");

        let code = "#pragma fuses NOCPD";
        assert_eq!(get_fuses(code).unwrap(), "#pragma config CPD = OFF\n");

        let code = "#pragma fuses PROTECT";
        assert_eq!(get_fuses(code).unwrap(), "#pragma config CP = ON\n");
    }

    #[test]
    fn cfg_multi_line() {
        let code = "#pragma fuses HS\n#pragma fuses NOCPD";
        assert_eq!(
            get_fuses(code).unwrap(),
            "#pragma config FOSC = HS\n#pragma config CPD = OFF\n"
        );
    }
    #[test]
    fn cfg_multiple() {
        let code = "#pragma fuses HS, PROTECT";
        assert_eq!(
            get_fuses(code).unwrap(),
            "#pragma config FOSC = HS\n#pragma config CP = ON\n"
        );
    }
}
