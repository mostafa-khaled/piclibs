use std::{
    io,
    path::{Path, PathBuf},
};

use crate::{
    pragmas::{
        fuses::*,
        interrupt::{generate_doc as generate_isr_doc, get_int_handlers},
        pragma_to_flags::get_pragma_compile_flags,
    },
    Error, Result,
};

const HAL_MEMEBERS: [(&str, &str); 5] = [
    ("gpio", include_str!("../../gpio.c")),
    ("uart", include_str!("../../uart.c")),
    ("timer", include_str!("../../timer.c")),
    ("interrupt", include_str!("../../interrupt.c")),
    ("adc", include_str!("../../adc.c")),
];

const HEADERS: [(&str, &str); 8] = [
    ("gpio", include_str!("../../gpio.h")),
    ("uart", include_str!("../../uart.h")),
    ("types", include_str!("../../types.h")),
    ("prelude", include_str!("../../prelude.h")),
    ("delay", include_str!("../../delay.h")),
    ("timer", include_str!("../../timer.h")),
    ("interrupt", include_str!("../../interrupt.h")),
    ("adc", include_str!("../../adc.h")),
];

const PRELUDE: &str = include_str!("../../prelude.h");

/// TODO: change this thing
const FLAGS: [&str; 4] = [
    "-DGETC_IS_UART_GETC",
    "-DPUTC_IS_UART_PUTC",
    "-mconst-data-in-progmem",
    "-I.",
];

/// a constant that holds the set of compile flags that does not depend on the system. which does
/// not include the device headers.
pub const FIXED_CLANG_FLAGS: &str = include_str!("../../fixed_compile_flags.txt");

pub const DEFAULT_MAIN: &str = include_str!("../../examples/blink-delay.c");

fn hal_files() -> impl Iterator<Item = PathBuf> {
    HAL_MEMEBERS
        .iter()
        .copied()
        .map(|(file, _)| Path::new(file).with_extension("c"))
}

fn hal_sources() -> impl Iterator<Item = &'static str> {
    HAL_MEMEBERS.iter().copied().map(|(_, source)| source)
}

pub fn xc8_include_dirs<P: AsRef<Path>>(root: P) -> [PathBuf; 3] {
    let inc_root = root.as_ref().join("pic").join("include");
    let c99 = inc_root.join("c99");
    let proc = inc_root.join("proc");
    [inc_root, c99, proc]
}

/// dumps the header at the root Path.
/// NOTE: this overwrites any existing files with the same name.
/// TODO: back-up existing files or return an error.
pub fn dump_header_files<P: AsRef<Path>>(root: P, dev_file: &str) -> io::Result<()> {
    use std::fs;
    fs::create_dir_all(root.as_ref())?;
    HEADERS.iter().try_for_each(|(name, content)| {
        let path = root.as_ref().join(name).with_extension("h");
        fs::write(path, content)
    });
    fs::write(root.as_ref().join(dev_file).with_extension("h"), PRELUDE)?;
    Ok(())
}

/// dumps the c code at the root Path.
pub fn dump_source_files<P: AsRef<Path>>(root: P) -> io::Result<()> {
    use std::fs;
    fs::create_dir_all(root.as_ref())?;
    HAL_MEMEBERS.iter().try_for_each(|(name, content)| {
        let path = root.as_ref().join(name).with_extension("c");
        fs::write(path, content)
    })
}

/// compiles the main (linking against the hal library).
/// TODO: add support for alternative hal files this helps when editing the hal itself
pub(crate) fn compile_main<P1: AsRef<Path>, P2: AsRef<Path>, P3: AsRef<Path>, P4: AsRef<Path>>(
    main: P1,
    output_file: P2,
    cc: P3,
    tmp: P4,
    dev_file: &str,
) -> Result<()> {
    use std::{env, fs, iter, process::Command};

    let build_dir = tmp.as_ref();

    dump_header_files(build_dir, dev_file)?;
    dump_source_files(build_dir)?;

    // We change the directory so we need to copy the main path because it can be a relative
    // path and we can't use it after the change of directory. This is not the best solution
    // but it is easy.
    let main_path = main.as_ref().canonicalize()?;

    // collect isrs from files.
    let main = fs::read_to_string(&main_path)?;
    let ints = HAL_MEMEBERS
        .iter()
        .copied()
        .map(|(_, src)| src)
        .chain(iter::once(main.as_str()))
        .flat_map(|src| get_int_handlers(src).into_iter())
        .collect();

    let int_handler = generate_isr_doc(ints)?;

    let int_handler_path = build_dir.join("auto_generated_isr").with_extension("c");

    fs::write(
        build_dir.join("auto_generated_isr").with_extension("c"),
        int_handler,
    )?;

    let int_handler_path = int_handler_path.canonicalize()?;

    // touch the output file so that canonicalize does not return an error
    fs::File::create(&output_file)?;
    let output_path = output_file.as_ref().canonicalize()?;
    let output_hex = Path::new("output").with_extension("hex");

    env::set_current_dir(build_dir)?;

    let extra_flags = hal_sources()
        .flat_map(get_pragma_compile_flags)
        .collect::<Vec<_>>();

    let main_extra_flags = get_pragma_compile_flags(&main);

    // extract fuses from main
    let fuses = get_fuses(&main).map_err(Error::UnknownFuses)?;

    fs::write("auto_generated_fuses.c", fuses)?;

    Command::new(cc.as_ref())
        .args(hal_files())
        .args(FLAGS)
        .arg(int_handler_path)
        .arg("auto_generated_fuses.c")
        .arg(&main_path)
        .args(extra_flags.into_iter())
        .args(main_extra_flags)
        .arg(format!("-mcpu={dev_file}"))
        .arg("-o")
        .arg(&output_hex)
        .spawn()?
        .wait()?;

    fs::copy(&output_hex, output_path)?;
    Ok(())
}
