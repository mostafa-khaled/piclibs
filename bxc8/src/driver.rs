use clap::Command;
use clap_complete::*;
use std::{io, path::PathBuf};

#[derive(clap::Parser)]
#[allow(unused)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Option<CliCommand>,
    #[arg(long = "generate", value_enum)]
    pub generator: Option<Shell>,
    #[clap(long, value_name = "XC8", env = "XC8")]
    pub xc8: Option<PathBuf>,
    #[clap(long, value_name = "DEVICE")]
    pub device: Option<String>,
}

#[derive(clap::Subcommand)]
#[allow(unused)]
pub enum CliCommand {
    Init {
        hal_dump: bool,
    },
    New {
        name: PathBuf,
        // hal_dump: bool,
    },
    Compile {
        #[clap(short, long, value_name = "FILE")]
        /// main file to be compiled.
        input: PathBuf,
        #[clap(short, long, value_name = "FILE")]
        output: Option<PathBuf>,
        #[clap(long, value_name = "FILE")]
        /// alternative hal implementation directory (used for development). currently ignored.
        source_dir: Option<PathBuf>,
        #[clap(long, value_name = "FILE")]
        /// alternative header files (used for development). currently ignored.
        header_dir: Option<PathBuf>,
        #[clap(short, long, value_name = "DIRECTORY", default_value = ".build")]
        build_dir: PathBuf,
    },
}

pub fn xc8_root() -> io::Result<Option<PathBuf>> {
    use std::path::Path;
    let path = Path::new("/opt/microchip/xc8/");
    if path.try_exists()? {
        let root = path.read_dir()?.find_map(|p| match p {
            Ok(p) => {
                if p.file_name().to_str()?.starts_with("v2") {
                    Some(p.path())
                } else {
                    None
                }
            }
            Err(_) => None,
        });
        Ok(root)
    } else {
        Ok(None)
    }
}

pub fn print_completions<G: Generator>(gen: G, cmd: &mut Command) {
    generate(gen, cmd, cmd.get_name().to_string(), &mut io::stdout());
}
