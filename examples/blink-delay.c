#include "prelude.h"

void main() {
  while (1) {
    output_high(PIN_B0);
    delay_ms(500);
    output_low(PIN_B0);
    delay_ms(500);
  }
}
