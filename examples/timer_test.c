#include "config.h"

#include "prelude.h"

void main() {
  input(PIN_B6);
  setup_timer_1(T1_EXTERNAL | T1_DIV_BY_1);
  while (1) {
    output_bit(PIN_B0, get_timer1() & 1);
  }
}
