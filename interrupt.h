#ifndef INTERRUPTS_H_
#define INTERRUPTS_H_

#include "types.h"
#include "timer.h"

#define TIMER0 0
#define INT_TIMER0 TIMER0
#define TIMER1 1
#define INT_TIMER1 TIMER1
#define TIMER2 2
#define INT_TIMER2 TIMER2
#define RB 4
#define INT_RB RB
#define EXT 8
#define INT_EXT EXT
#define TX 16
#define INT_TX TX
#define RC 32
#define INT_RC RC
#define GLOBAL 128

#define L_TO_H 1
#define H_TO_L 0

void enable_interrupts(uint8 interrupts);
void disable_interrupts(uint8 interrupts);
int8 interrupts_enabled(uint8 interrupt);
void ext_int_edge(uint8 edge);
int8 interrupt_active(uint8 interrupt);


#endif // INTERRUPTS_H_
