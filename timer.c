#include "timer.h"
#include <xc.h>

void setup_timer_0(uint8 mode) {
  OPTION_REG = (OPTION_REG & 0xc0) | (mode & 0x3f);
  set_timer0(0);
}

void set_timer0(uint8 value) { TMR0 = value; }

void set_rtcc(uint8 value) { set_timer0(value); }

uint8 get_timer0() { return TMR0; }

void setup_timer_1(uint8 mode) {
  T1CON = mode & 0x3f;
  set_timer1(0);
}

void set_timer1(uint16 value) { TMR1 = value; }

uint16 get_timer1() { return TMR1; }

void setup_timer_2(uint8 mode, uint8 period, uint8 postscale) {
  PR2 = period;
  mode &= 0x07;
  postscale &= 0x0f;
  postscale <<= 3;
  T2CON = mode | postscale;
  set_timer2(0);
}

void set_timer2(uint8 value) { TMR2 = value; }

uint8 get_timer2() { return TMR2; }
