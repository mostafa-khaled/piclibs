#ifndef UART_H_
#define UART_H_

#include "types.h"

#ifndef UART_SINGLE_BUFFER_SIZE
#define UART_SINGLE_BUFFER_SIZE 16
#endif

#if UART_SINGLE_BUFFER_SIZE > 255
#error "RECEIVE_BUFFER_SIZE must be less than 255"
#endif

// sets the speed of the UART.
void setup_uart(int32 baud);

// sets the speed of the UART.
void setup_uart_speed(int32 baud);

// returns the number of bytes in the receive buffer.
uint8 rcv_buffer_bytes();

// returns 1 if the receive buffer is full.
int rcv_buffer_full();

// returns the number of bytes in the transmit buffer.
uint8 tx_buffer_bytes();

// returns 1 if the transmit buffer is full.
int tx_buffer_full();

// sends a single char through uart.
void uart_putc(char c);

// sends a (null treminated) string through uart.
void uart_puts(char *s);

// reads a single char from uart.
// NOTE: this is a blocking function.
char uart_getc();

#endif // UART_H_
