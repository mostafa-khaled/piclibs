#ifndef GPIO_H_
#define GPIO_H_

#include "types.h"
#include <xc.h>

typedef uint8 gpio_pin_t;

#ifdef PORTA
#define PIN_A0 0
#define PIN_A1 1
#define PIN_A2 2
#define PIN_A3 3
#define PIN_A4 4
#define PIN_A5 5
#define PIN_A6 6
#define PIN_A7 7
#endif

#ifdef PORTB
#define PIN_B0 8
#define PIN_B1 9
#define PIN_B2 10
#define PIN_B3 11
#define PIN_B4 12
#define PIN_B5 13
#define PIN_B6 14
#define PIN_B7 15
#endif

#ifdef PORTC
#define PIN_C0 16
#define PIN_C1 17
#define PIN_C2 18
#define PIN_C3 19
#define PIN_C4 20
#define PIN_C5 21
#define PIN_C6 22
#define PIN_C7 23
#endif

// outputs high on the given pin (automatically converting it to output if it is
// not already.)
void output_high(gpio_pin_t pin);

// outputs low on the given pin (automatically converting it to output if it is
// not already.)
void output_low(gpio_pin_t pin);

void output_toggle(gpio_pin_t pin);

// sets the given pin to be an input (floating) pin.
void output_float(gpio_pin_t pin);

#ifdef PORTA
// outputs the given value on port A.
void output_a(int8 value);
#endif

#ifdef PORTB
// outputs the given value on port B.
void output_b(int8 value);
#endif

#ifdef PORTC
void output_c(int8 value);
#endif

// outputs the given value on the given pin.
void output_bit(gpio_pin_t pin, int8 value);

// returns 1 if the pin is high, or 0 if it is low. if the input is not
// configured as input it is automatically converted to be an input pin.
int8 input(gpio_pin_t pin);

// reads the status of the pin without chaning its relevant tris bit.
int8 input_state(gpio_pin_t pin);

#ifdef PORTA
// compares the currentn state of the register A and returns 1 if it is changed.
int8 input_change_a();
#endif

#ifdef PORTB
// compares the currentn state of the register B and returns 1 if it is changed.
int8 input_change_b();
#endif

#ifdef PORTC
// compares the currentn state of the register C and returns 1 if it is changed.
int8 input_change_c();
#endif

#ifdef PORTA
// sets the tris bits for port A, 0 means Output, 1 means Input
void set_tris_a(uint8 value);
#endif

#ifdef PORTB
// sets the tris bits for port B, 0 means Output, 1 means Input
void set_tris_b(uint8 value);
#endif

#ifdef PORTC
// sets the tris bits for port C, 0 means Output, 1 means Input
void set_tris_c(uint8 value);
#endif

#ifdef nRBPU_bit
// enables/disables the pull-ups on port B
void port_b_pullups(int8 en);
#endif

#ifdef nRCPU_bit
// enables/disables the pull-ups on port C
void port_c_pullups(int8 en);
#endif

#endif // GPIO_H_
