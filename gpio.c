#include "gpio.h"
#include <xc.h>

#ifdef PORTA
static uint8 LAST_A_READ = 0;
#endif

#ifdef PORTB
static uint8 LAST_B_READ = 0;
#endif

#ifdef PORTC
static uint8 LAST_C_READ = 0;
#endif

#ifdef PORTA
static int is_port_a(gpio_pin_t pin) {
  if (pin >= PIN_A0 && pin <= PIN_A7) {
    return 1;
  } else {
    return 0;
  }
}
#endif

#ifdef PORTB
static int is_port_b(gpio_pin_t pin) {
  if (pin >= PIN_B0 && pin <= PIN_B7) {
    return 1;
  } else {
    return 0;
  }
}
#endif

#ifdef PORTC
static int is_port_c(gpio_pin_t pin) {
  if (pin >= PIN_C0 && pin <= PIN_C7) {
    return 1;
  } else {
    return 0;
  }
}
#endif

// ensures that the pin is set to output
static void ensure_output(gpio_pin_t pin) {
#ifdef PORTA
  if (is_port_a(pin)) {
    TRISA &= ~(1 << (pin - PIN_A0));
  }
#endif
#ifdef PORTB
  if (is_port_b(pin)) {
    TRISB &= ~(1 << (pin - PIN_B0));
  }
#endif
#ifdef PORTC
  if (is_port_c(pin)) {
    TRISC &= ~(1 << (pin - PIN_C0));
  }
#endif
}

static void ensure_input(gpio_pin_t pin) {
#ifdef PORTA
  if (is_port_a(pin)) {
    TRISA |= 1 << (pin - PIN_A0);
  }
#endif
#ifdef PORTB
  if (is_port_b(pin)) {
    TRISB |= 1 << (pin - PIN_B0);
  }
#endif
#ifdef PORTC
  if (is_port_c(pin)) {
    TRISC |= 1 << (pin - PIN_C0);
  }
#endif
}

void output_bit(gpio_pin_t pin, int8 val) {
#ifdef PORTA
  if (is_port_a(pin)) {
#if defined(STANDARD_IO_A) || !defined(FAST_IO_A)
    ensure_output(pin);
#endif
    if (val == 1) {
      PORTA |= 1 << (pin - PIN_A0);
    } else {
      PORTA &= ~(1 << (pin - PIN_A0));
    }
  }
#endif

#ifdef PORTB
  if (is_port_b(pin)) {
#if defined(STANDARD_IO_B) || !defined(FAST_IO_B)
    ensure_output(pin);
#endif
    if (val == 1) {
      PORTB |= 1 << (pin - PIN_B0);
    } else {
      PORTB &= ~(1 << (pin - PIN_B0));
    }
  }
#endif

#ifdef PORTC
  if (is_port_c(pin)) {
#if defined(STANDARD_IO_C) || !defined(FAST_IO_C)
    ensure_output(pin);
#endif
    if (val == 1) {
      PORTC |= 1 << (pin - PIN_C0);
    } else {
      PORTC &= ~(1 << (pin - PIN_C0));
    }
  }
#endif
}

void output_high(gpio_pin_t pin) { output_bit(pin, 1); }

void output_low(gpio_pin_t pin) { output_bit(pin, 0); }

void output_toggle(gpio_pin_t pin) {
  // we use input_state since it does not change the tris register
  if (input_state(pin) == 1) {
    output_low(pin);
  } else {
    output_high(pin);
  }
}

void output_float(gpio_pin_t pin) { ensure_input(pin); }

#ifdef PORTA
void output_a(int8 value) { PORTA = value; }
#endif

#ifdef PORTB
void output_b(int8 value) { PORTB = value; }
#endif

#ifdef PORTC
void output_c(int8 value) { PORTC = value; }
#endif

int8 input(gpio_pin_t pin) {
#ifdef PORTA
  if (is_port_a(pin)) {
#if defined(STANDARD_IO_A) || !defined(FAST_IO_A)
    ensure_input(pin);
#endif
    return input_state(pin);
  }
#endif

#ifdef PORTB
  if (is_port_b(pin)) {
#if defined(STANDARD_IO_B) || !defined(FAST_IO_B)
    ensure_input(pin);
#endif
    return input_state(pin);
  }
#endif

#if defined(PORTC)
  if (is_port_c(pin)) {
#if defined(STANDARD_IO_C) || !defined(FAST_IO_C)
    ensure_input(pin);
#endif
    return input_state(pin);
  }
#endif

  return 0;
}

#ifdef nRBPU_bit
// enables/disables the pull-ups on port B
void port_b_pullups(int8 en) {
  if (en == 1) {
    nRBPU = 0;
  } else {
    nRBPU = 1;
  }
}
#endif

#ifdef nRCPU_bit
// enables/disables the pull-ups on port C
void port_c_pullups(int8 en) {
  if (en == 1) {
    nRBPU = 0;
  } else {
    nRBPU = 1;
  }
}
#endif

int8 input_state(gpio_pin_t pin) {
#ifdef PORTA
  if (is_port_a(pin)) {
    return (PORTA & (1 << (pin - PIN_A0))) ? 1 : 0;
  }
#endif

#ifdef PORTB
  if (is_port_b(pin)) {
    return (PORTB & (1 << (pin - PIN_B0))) ? 1 : 0;
  }
#endif

#ifdef PORTC
  if (is_port_c(pin)) {
    return (PORTC & (1 << (pin - PIN_C0))) ? 1 : 0;
  }
#endif

  return 0;
}

#ifdef PORTA
int8 input_change_a() {
  uint8 current = PORTA;
  int8 result = current == LAST_A_READ ? 0 : 1;
  LAST_A_READ = current;
  return result;
}
#endif

#ifdef PORTB
int8 input_change_b() {
  uint8 current = PORTB;
  int8 result = current == LAST_B_READ ? 0 : 1;
  LAST_B_READ = current;
  return result;
}
#endif

#ifdef PORTC
int8 input_change_c() {
  uint8 current = PORTC;
  int8 result = current == LAST_C_READ ? 0 : 1;
  LAST_C_READ = current;
  return result;
}
#endif

#ifdef PORTA
void set_tris_a(uint8 val) { TRISA = val; }
#endif

#ifdef PORTB
void set_tris_b(uint8 val) { TRISB = val; }
#endif

#ifdef PORTC
void set_tris_c(uint8 val) { TRISC = val; }
#endif
